﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

using LitJson;

public class restTest : MonoBehaviour {

	
	public IEnumerator get()
	{
		// Execute request
		WWW www = new WWW("http://localhost:3000/hiscores");
		yield return www;

		// Process response
		JsonData responseData = JsonMapper.ToObject(www.text);
		Debug.Log ("count " + responseData.Count);
		string newText = "";
		for(int i = 0; i < responseData.Count; i++)
		{
			newText += responseData[i]["value"] + ", " + responseData[i]["name"] + "\n";

		}

		// Add the results to the screen 
		GameObject o = GameObject.Find("GetResults");
		o.GetComponent<Text>().text = newText;
	}

	public IEnumerator post(string name, int value)
	{
		// Literal string
//		string test = @"{""hiscore"":{""value"":100,""name"":""Max""}}";

		// String concatenation
		string test = "{\"hiscore\":{\"value\":" + value + ",\"name\":\"" + name + "\"}}";
		byte[] postData = System.Text.Encoding.ASCII.GetBytes(test);

		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("Content-Type", "application/json");
		

		WWW www = new WWW("http://localhost:3000/hiscores", postData, headers);
		yield return www;
		
	}

	public void postData()
	{
		GameObject o = GameObject.Find("InputFieldName");
		string name = o.GetComponent<InputField>().text;

		o = GameObject.Find("InputFieldValue");
		int value = int.Parse(o.GetComponent<InputField>().text);

		StartCoroutine(post(name, value));
	}



	public void getData()
	{
		StartCoroutine(get());
	}
}
