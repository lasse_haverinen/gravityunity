﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class MinMaxRange{
	public float minRange;
	public float maxRange;
}

public class AsteroidSpawner : MonoBehaviour {

	public GameObject asteroid;
	public GameObject asteroidManager;
	public MinMaxRange spawnRange;
	public float spawnInterval = 3.0f;
	public int maxAsteroids = 100;
	private int currentAsteroids = 0;


	void Start()
	{
		Invoke("CreateMyInstanceLoop", 3.0f);
	}



	// Will be called 3 seconds after level start
	void CreateMyInstanceLoop()
	{			
		if(currentAsteroids < maxAsteroids)
		{
			// get the angle for this step (in radians, not degrees)
			float angle = Mathf.PI * UnityEngine.Random.Range(0.0f, 2.0f);
			
			// the X &amp; Y position for this angle are calculated using Sin &amp; Cos
			float range = UnityEngine.Random.Range(spawnRange.minRange, spawnRange.maxRange);
			float x = Mathf.Sin(angle) * range;
			float y = Mathf.Cos(angle) * range;
			
			Vector3 pos = new Vector3(x, 0.0f, y) ;
			
			// no need to assign the instance to a variable unless you're using it afterwards:
			GameObject n = (GameObject)Instantiate (asteroid, pos, Quaternion.identity);
			n.transform.parent = asteroidManager.transform;

			currentAsteroids++;
		}
					
		Invoke("CreateMyInstanceLoop", spawnInterval);
	}
}
