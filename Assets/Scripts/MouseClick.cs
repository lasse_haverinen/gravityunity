﻿using UnityEngine;
using System.Collections;


public class MouseClick : MonoBehaviour {

	public Camera gameCam;

	public GameObject weapon1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0)){
			//Debug.Log("mouseclick");
			CastRayToWorld();

		}
	}

	Vector3 getSelectedPlanetPos()
	{
		GameObject planets = GameObject.Find("SystemPlanets");

		foreach(Transform child in planets.transform)
		{
			SelectObject scr = (SelectObject)child.gameObject.GetComponent("SelectObject");

			if(scr != null && scr.getIsSelected())
			{
				return child.position;
			}

		}

		return new Vector3();

	}

	void CastRayToWorld() 
	{
		Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
		Vector3 point = r.origin + (r.direction * gameCam.transform.position.y);
		point.y = 0.0f;

		Vector3 from = getSelectedPlanetPos();
		if(from.x == 0.0f && from.z == 0.0f)
		{
			return;
		}


		GameObject n = (GameObject)Instantiate(weapon1, from, Quaternion.identity);
		WeaponController compo = (WeaponController)(n.transform.GetComponent("WeaponController"));
		compo.FireAt(point);


		//var ray : Ray = Camera.main.ScreenPointToRay(Input.mousePosition);    
		//var point : Vector3 = ray.origin + (ray.direction * distance);    
		//Debug.Log( "World point " + point );
	}
}
