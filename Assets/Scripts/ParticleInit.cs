﻿using UnityEngine;
using System.Collections;

public class ParticleInit : MonoBehaviour {

	public Vector3 velocity;

	// Use this for initialization
	void Start () {
		if(velocity.magnitude == 0.0f)
		{
			transform.rigidbody.velocity = new Vector3(Random.Range(-20.0f, 20.0f), 0.0f, Random.Range(-20.0f, 20.0f));
		}
		else
		{
			transform.rigidbody.velocity = velocity;
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
