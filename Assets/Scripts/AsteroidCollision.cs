﻿using UnityEngine;
using System.Collections;

public class AsteroidCollision : MonoBehaviour {

	public GameObject explosionObject;

	/*void OnTriggerEnter(Collider other) 
	{
		GameObject o = (GameObject)Instantiate(explosionObject);
		o.transform.position = transform.position;
		Destroy(gameObject);
	}*/

	void OnCollisionEnter(Collision collisionInfo)
	{
		Vector3 myCollisionNormal = collisionInfo.contacts[0].normal;

		GameObject o = (GameObject)Instantiate(explosionObject);
		o.transform.position = transform.position;
		o.transform.Rotate(myCollisionNormal, Space.World);



		Destroy(gameObject);
	}
}
