﻿using UnityEngine;
using System.Collections;

public class SelectObject : MonoBehaviour {

	public GameObject selectorHighlight;

	public bool isActivated = false;

	void OnMouseDown() {
		isActivated = !isActivated;
		visualizeActivation();


	}

	void Start()
	{
		visualizeActivation();
	}

	public bool getIsSelected()
	{
		return isActivated;
	}

	void visualizeActivation()
	{
		if(isActivated == true)
		{
			GameObject select = (GameObject)Instantiate(selectorHighlight, transform.position, Quaternion.Euler(90.0f, 0.0f, 0.0f));
			
			select.transform.name = "SelectHighlight";
			select.transform.parent = transform;
			select.transform.localScale = new Vector3(10.0f, 10.0f, 10.0f);
		}
		else
		{
			Transform x = transform.Find("SelectHighlight");
			if(x != null)
			{
				Destroy(x.gameObject);
			}
		}
	}




}
