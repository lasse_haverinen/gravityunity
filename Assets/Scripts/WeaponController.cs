﻿using UnityEngine;
using System.Collections;

public class WeaponController : MonoBehaviour {

	public float maxImpulseForce = 2.0f;
	private Vector3 myTarget;
	public GameObject explosionEffect;
	public Object gameController;

	// Use this for initialization
	void Start () {		
	}

	public void FireAt(Vector3 target) {
		//transform.position = from;
		myTarget = target;
		transform.LookAt(target);
		rigidbody.AddForce(transform.forward * maxImpulseForce);
	}
	
	// Update is called once per frame
	void Update () {
		float distance = Vector3.Distance(transform.position, myTarget);
		/*if(transform.position == myTarget)
		{
			transform.rigidbody.velocity = new Vector3();
			Debug.Log("Target reached");
		}*/
		if(distance < 2.0f)
		{
			//transform.rigidbody.velocity = new Vector3();
			Instantiate(explosionEffect, transform.position, Quaternion.identity);

			GameObject asteroids = GameObject.Find("GravityPulled");			
			foreach(Transform a in asteroids.transform)
			{
				float asteroidDistance = Vector3.Distance(transform.position, a.position);
				if(asteroidDistance <= 10.0f)
				{
					Destroy (a.gameObject);
				}
			}


			Destroy(gameObject);


		}

	}

	/*void OnTriggerEnter(Collider c)
	{
		Debug.Log("Collision");
	}*/
}
