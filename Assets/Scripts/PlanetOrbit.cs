﻿using UnityEngine;
using System.Collections;

public class PlanetOrbit : MonoBehaviour {

	/* speed of orbit (in degrees/second) */
	public float speedFactor;
	public GameObject nonParentGravityBody;

	private Transform target;
	private float speed;


	public void Start()
	{
		if(nonParentGravityBody == null)
		{
			target = transform.parent;
		}
		else
		{
			target = nonParentGravityBody.transform;
		}

		speed = speedFactor / Vector3.Distance(transform.position, target.position);
	}

	
	public void Update()
	{
		speed = speedFactor / Vector3.Distance(transform.position, target.position);

		if (target != null) {
			transform.RotateAround(target.position, Vector3.up, speed * Time.deltaTime);
			transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
		}
	}
}
