﻿using UnityEngine;
using System.Collections;

public class GravityPull : MonoBehaviour {

	public GameObject pulledGroup;
	public float mass = 1e+08f;
	public float gravityMultiplier = 0.01f;

	// Use this for initialization
	void FixedUpdate() {

		if(pulledGroup == null)
		{
			return;
		}

		int children = pulledGroup.transform.childCount;
		for (int i = 0; i < children; ++i)
		{
			Transform x = pulledGroup.transform.GetChild(i);
			// Gets a vector that points from the player's position to the target's.
			// target.position - player.position
			Vector3 heading = transform.position - x.position;

			float distance = heading.magnitude;
			Vector3 direction = heading.normalized; 

			float pullerMass = transform.localScale.x * mass;
			//float pulledMass = x.rigidbody.mass;

			//float magnitude = 6.67f * Mathf.Pow(10.0f, -11.0f) * pullerMass * pulledMass / distance;
			float magnitude = 6.67e-11f * pullerMass / distance;
			magnitude *= gravityMultiplier;
			//Debug.Log(magnitude);


			Vector3 gravityForce = direction * magnitude;




			x.gameObject.rigidbody.AddForce(gravityForce);


			//Debug.DrawLine(x.position, transform.position);


		}

		//rigidbody.AddForce(Vector3.up * 10);

	}
}
